"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _react = _interopRequireWildcard(require("react"));
var _reactRouterDom = require("react-router-dom");
var _logo_app = _interopRequireDefault(require("../../image/logo_app.png"));
var _icon_tester = _interopRequireDefault(require("../../image/icon_tester.png"));
var _indexModule = _interopRequireDefault(require("./styles/index.module.css"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }
function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }
/* eslint-disable jsx-a11y/mouse-events-have-key-events */

var HatBar = function HatBar(_ref) {
  var codeAdmin = _ref.codeAdmin;
  var navigationHeader = function navigationHeader() {
    if (codeAdmin === '111') {
      return /*#__PURE__*/_react.default.createElement("ul", null, /*#__PURE__*/_react.default.createElement("li", null, /*#__PURE__*/_react.default.createElement(_reactRouterDom.Link, {
        to: "/createTask"
      }, "\u0421\u043E\u0437\u0434\u0430\u0442\u044C \u0437\u0430\u0434\u0430\u0447\u0443")), /*#__PURE__*/_react.default.createElement("li", null, /*#__PURE__*/_react.default.createElement(_reactRouterDom.Link, {
        to: "/testers"
      }, "\u0422\u0435\u0441\u0442\u0438\u0440\u043E\u0432\u0449\u0438\u043A\u0438")), /*#__PURE__*/_react.default.createElement("li", null, /*#__PURE__*/_react.default.createElement(_reactRouterDom.Link, {
        to: "/managers"
      }, "\u0420\u0443\u043A\u043E\u0432\u043E\u0434\u0438\u0442\u0435\u043B\u0438")));
    }
    return null;
  };
  return /*#__PURE__*/_react.default.createElement("header", {
    className: _indexModule.default.wrapper_header
  }, /*#__PURE__*/_react.default.createElement(_reactRouterDom.Link, {
    to: "/home",
    className: _indexModule.default.wrapper_logo
  }, /*#__PURE__*/_react.default.createElement("img", {
    alt: "logo_app",
    src: _logo_app.default,
    className: _indexModule.default.logo_app
  }), /*#__PURE__*/_react.default.createElement("p", {
    className: _indexModule.default.label_app
  }, "Yes Test")), /*#__PURE__*/_react.default.createElement("div", {
    className: _indexModule.default.wrapper_nav_search
  }, /*#__PURE__*/_react.default.createElement("nav", {
    className: _indexModule.default.wrapper_nav
  }, navigationHeader())), /*#__PURE__*/_react.default.createElement("div", {
    className: _indexModule.default.wrapper_infoUser
  }, /*#__PURE__*/_react.default.createElement(_reactRouterDom.Link, {
    to: "/userPage"
  }, /*#__PURE__*/_react.default.createElement("img", {
    alt: "iconUser",
    src: _icon_tester.default,
    className: _indexModule.default.icon_User
  }))));
};
var _default = HatBar;
exports.default = _default;