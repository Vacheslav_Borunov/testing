// @ts-nocheck
import React, { useEffect, useState } from 'react';
import { observer } from 'mobx-react'
import { Routes, Route } from 'react-router-dom'
import Home from './pages/Home/Home';
import HatBar from './components/HatBar';
import FooterBar from './components/FooterBar';
import Managers from './pages/Managers/Managers';
import CreateTask from './pages/CreateTask/CreateTask';
import NotfoundPage from './pages/NotfoundPage/NotfoundPage';
import UserPage from './pages/UserPage/UserPage';
import TaskPage from './pages/TaskPage/TaskPage';
import Results from './pages/Testers/Results';
import Entry from './pages/Entry/Entry';

import styles from './app.module.css';

const App = observer(() => {
  const [ users, setUsers ] = useState([])

  const [ admins, setAdmins ] = useState([])

  const [ currentUser, serCurrentUser ] = useState({})

  const [ taskQuestion, setTaskQuestion ] = useState([])

  const [ taskPassedQuestion, setTaskPassedQuestion ] = useState([])

  useEffect(() => {
    if (localStorage.getItem('users') !== null) {
      const localUsers = JSON.parse(localStorage.getItem('users'))
      setUsers(localUsers)
    }
    if (localStorage.getItem('admins') !== null) {
      const localAdmins = JSON.parse(localStorage.getItem('admins'))
      setAdmins(localAdmins)
    }
    if (localStorage.getItem('currentUser') !== null) {
      const localCurrentUser = JSON.parse(localStorage.getItem('currentUser'))
      serCurrentUser(...localCurrentUser)
    }
    if (localStorage.getItem('taskQuestion') !== null) {
      const localTaskQuestion = JSON.parse(localStorage.getItem('taskQuestion'))
      setTaskQuestion(localTaskQuestion)
    }

    if (localStorage.getItem('taskPassedQuestion') !== null) {
      const localTaskPassedQuestion = JSON.parse(localStorage.getItem('taskPassedQuestion'))
      setTaskPassedQuestion(localTaskPassedQuestion)
    }
  }, [])

  return (
    <div className={ styles.fon }>
      <HatBar currentUserId={ currentUser.id } admins={ admins } />
      <Routes>
        <Route
          path="/"
          element={ (
            <Entry
              users={ users }
              setUsers={ setUsers }
              admins={ admins }
              setAdmins={ setAdmins }
              serCurrentUser={ serCurrentUser }
            />
        ) }
        />
        <Route
          path="/home"
          element={ (
            <Home taskQuestion={ taskQuestion } />
        ) }
        />
        <Route
          path="/createTask"
          element={ (
            <CreateTask taskQuestion={ taskQuestion } setTaskQuestion={ setTaskQuestion } />
        ) }
        />
        <Route
          path="/managers"
          element={ (
            <Managers />
        ) }
        />
        <Route
          path="/results"
          element={ (
            <Results taskPassedQuestion={ taskPassedQuestion } />
        ) }
        />
        <Route
          path="/userPage"
          element={ (
            <UserPage currentUser={ currentUser } />
        ) }
        />
        <Route
          path="/taskPage"
          element={ (
            <TaskPage
              taskQuestion={ taskQuestion }
              currentUser={ currentUser }
              taskPassedQuestion={ taskPassedQuestion }
              setTaskPassedQuestion={ setTaskPassedQuestion }
            />
        ) }
        />
        <Route path="*" element={ <NotfoundPage /> } />
      </Routes>
      <FooterBar />
    </div>
  )
})

export default App;
