// @ts-nocheck
import React, { FC, useState } from 'react';
import {
  Alert,
  AlertDescription,
  AlertIcon,
  AlertTitle,
  Button, Input, Radio, RadioGroup, Select, Stack, Textarea,
} from '@chakra-ui/react'

import styles from './styles/index.module.css'

const LABELS = {
  CREATE_TASK: 'Создание нового теста',
}

interface Props {
  setTaskQuestion: (params:{
    time: number,
    taskName: string,
    taskDescription: string,
    taskTester: string,
    type: number,
    testCheck: number,
    taskComment: string,
  }[]) => void,
  taskQuestion: {
    idCategory: number,
    category: string,
    questionList : {
      idQuestion: number,
      question: string,
      checkedAnswer: number,
      answerList : {
        idAnswer: number,
        answer: string,
      }[],
    }[],
  }[],
}

const CreateTask: FC<Props> = ({ setTaskQuestion, taskQuestion }) => {
  const [ flagAlarmError, setFlagAlarmError ] = useState<boolean>(false)

  const [ questionList, setQuestionList ] = useState([])

  const [ category, setCategory ] = useState<string>('')
  const [ question, setQuestion ] = useState<string>('')

  const [ answerOne, setAnswerOne ] = useState<string>('')
  const [ answerTwo, setAnswerTwo ] = useState<string>('')
  const [ answerThree, setAnswerThree ] = useState<string>('')
  const [ answerFour, setAnswerFour ] = useState<string>('')
  const [ answerFive, setAnswerFive ] = useState<string>('')

  const [ checkedAnswer, setCheckedAnswer ] = useState('1')

  const [ btnFlag, setBtnFlag ] = useState(2)

  const ruleCreateQuestion = () => {
    const date = new Date().getTime()

    if (answerFive !== '') {
      if (answerOne === '' || answerTwo === '' || answerThree === '' || answerFour === '') {
        return setFlagAlarmError(true)
      }
      setFlagAlarmError(false)
      return (
        setQuestionList([ ...questionList, {
          idQuestion: date,
          question,
          checkedAnswer,
          answerList: [{
            idAnswer: 1,
            answer: answerOne,
          },
          {
            idAnswer: 2,
            answer: answerTwo,
          },
          {
            idAnswer: 3,
            answer: answerThree,
          },
          {
            idAnswer: 4,
            answer: answerFour,
          },
          {
            idAnswer: 5,
            answer: answerFive,
          },
          ],
        }])
      )
    }

    if (answerFour !== '') {
      if (answerOne === '' || answerTwo === '' || answerThree === '') {
        return setFlagAlarmError(true)
      }
      setFlagAlarmError(false)
      return (
        setQuestionList([ ...questionList, {
          idQuestion: date,
          question,
          checkedAnswer,
          answerList: [{
            idAnswer: 1,
            answer: answerOne,
          },
          {
            idAnswer: 2,
            answer: answerTwo,
          },
          {
            idAnswer: 3,
            answer: answerThree,
          },
          {
            idAnswer: 4,
            answer: answerFour,
          },
          ],
        }])
      )
    }

    if (answerThree !== '') {
      if (answerOne === '' || answerTwo === '') {
        return setFlagAlarmError(true)
      }
      setFlagAlarmError(false)
      return (
        setQuestionList([ ...questionList, {
          idQuestion: date,
          question,
          checkedAnswer,
          answerList: [{
            idAnswer: 1,
            answer: answerOne,
          },
          {
            idAnswer: 2,
            answer: answerTwo,
          },
          {
            idAnswer: 3,
            answer: answerThree,
          },
          ],
        }])
      )
    }

    if (answerOne === '' || answerTwo === '') {
      return setFlagAlarmError(true)
    }
    setFlagAlarmError(false)
    return (
      setQuestionList([ ...questionList, {
        idQuestion: date,
        question,
        checkedAnswer,
        answerList: [{
          idAnswer: 1,
          answer: answerOne,
        },
        {
          idAnswer: 2,
          answer: answerTwo,
        },
        ],
      }])
    )
  }
  const hendaleCreateQuestion = () => {
    ruleCreateQuestion()
    setQuestion('')
    setAnswerOne('')
    setAnswerTwo('')
    setAnswerThree('')
    setAnswerFour('')
    setAnswerFive('')
    setBtnFlag(2)
  }

  const hendaleCreateTask = () => {
    const date = new Date().getTime()

    setTaskQuestion([ ...taskQuestion, {
      idCategory: date,
      category,
      questionList,
    }])

    localStorage.setItem('taskQuestion', JSON.stringify([ ...taskQuestion, {
      idCategory: date,
      category,
      questionList,
    }]))

    setQuestionList([])
    setCategory('')
    setQuestion('')

    setQuestion('')
    setAnswerOne('')
    setAnswerTwo('')
    setAnswerThree('')
    setAnswerFour('')
    setAnswerFive('')
    setBtnFlag(2)
  }

  const alarmError = () => {
    if (flagAlarmError) {
      return (
        <Alert status="error">
          <AlertIcon />
          <AlertTitle>Вы не заполнили данные!</AlertTitle>
          <AlertDescription>Заполните все поля и повторите попытку.</AlertDescription>
        </Alert>
      )
    }
    return null
  }

  const btnAddAnswer = () => {
    if (btnFlag >= 5) {
      return null
    }
    return (
      <Button colorScheme="gray" onClick={ () => setBtnFlag(btnFlag + 1) }>
        Добавить вариант
      </Button>
    )
  }

  const renderItem = answerListItem => (
    answerListItem.map((el, index) => (
      <div className={ styles.radioBlock } key={ el.idAnswer }>
        <Radio isDisabled className={ styles.radio } colorScheme="green" value={ `${index + 1}` }>{el.answer}</Radio>
      </div>
    )))

  const renderQuestion = () => {
    if (questionList.length !== 0) {
      return (
        questionList.map(el => (
          <div key={ el.idQuestion }>
            <p>{ el.question }</p>
            <RadioGroup defaultValue={ el.checkedAnswer }>
              <Stack>
                { renderItem(el.answerList) }
              </Stack>
            </RadioGroup>
          </div>
        ))
      )
    }
    return null
  }

  return (
    <div>
      <section className={ styles.wrapper_moveList }>
        <div className={ styles.wrapper_posters }>
          <p className={ styles.wrapper_title }>{LABELS.CREATE_TASK}</p>
          <div className={ styles.wrapper_content }>
            <p>Категория</p>
            <Input
              placeholder="Категория"
              value={ category }
              onChange={ e => setCategory(e.target.value) }
            />
            { renderQuestion() }
            <p>Вопрос</p>
            <Textarea
              value={ question }
              onChange={ e => setQuestion(e.target.value) }
            />
            <p>Варианты ответа</p>
            <RadioGroup defaultValue="1">
              <Stack>
                <div className={ styles.radioBlock }>
                  <Radio className={ styles.radio } colorScheme="green" value="1" onChange={ e => setCheckedAnswer(e.target.value) } />
                  <Input
                    placeholder="Вариант ответа"
                    value={ answerOne }
                    onChange={ e => setAnswerOne(e.target.value) }
                  />
                  <Button colorScheme="gray" onClick={ () => setAnswerOne('') }>
                    &#10006;
                  </Button>
                </div>
                <div className={ styles.radioBlock }>
                  <Radio className={ styles.radio } colorScheme="green" value="2" onChange={ e => setCheckedAnswer(e.target.value) } />
                  <Input
                    placeholder="Вариант ответа"
                    value={ answerTwo }
                    onChange={ e => setAnswerTwo(e.target.value) }
                  />
                  <Button colorScheme="gray" onClick={ () => setAnswerTwo('') }>
                    &#10006;
                  </Button>
                </div>
                {btnFlag >= 3 ? (
                  <div className={ styles.radioBlock }>
                    <Radio className={ styles.radio } colorScheme="green" value="3" onChange={ e => setCheckedAnswer(e.target.value) } />
                    <Input
                      placeholder="Вариант ответа"
                      value={ answerThree }
                      onChange={ e => setAnswerThree(e.target.value) }
                    />
                    <Button colorScheme="gray" onClick={ () => setAnswerThree('') }>
                      &#10006;
                    </Button>
                  </div>
                ) : null}
                {btnFlag >= 4 ? (
                  <div className={ styles.radioBlock }>
                    <Radio className={ styles.radio } colorScheme="green" value="4" onChange={ e => setCheckedAnswer(e.target.value) } />
                    <Input
                      placeholder="Вариант ответа"
                      value={ answerFour }
                      onChange={ e => setAnswerFour(e.target.value) }
                    />
                    <Button colorScheme="gray" onClick={ () => setAnswerFour('') }>
                      &#10006;
                    </Button>
                  </div>
                ) : null}
                {btnFlag === 5 ? (
                  <div className={ styles.radioBlock }>
                    <Radio className={ styles.radio } colorScheme="green" value="5" onChange={ e => setCheckedAnswer(e.target.value) } />
                    <Input
                      placeholder="Вариант ответа"
                      value={ answerFive }
                      onChange={ e => setAnswerFive(e.target.value) }
                    />
                    <Button colorScheme="gray" onClick={ () => setAnswerFive('') }>
                      &#10006;
                    </Button>
                  </div>
                ) : null}
              </Stack>
            </RadioGroup>
            { alarmError() }
            { btnAddAnswer() }
            <Button colorScheme="yellow" onClick={ () => hendaleCreateQuestion() }>
              Подтвердить создание вопроса
            </Button>
            <Button colorScheme="yellow" onClick={ () => hendaleCreateTask() }>
              Подтвердить создание теста
            </Button>
          </div>
        </div>
      </section>
    </div>
  );
}

export default CreateTask;
