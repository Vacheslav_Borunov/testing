import React, { FC, useEffect, useState } from 'react';
import { Button } from '@chakra-ui/react';
import { Link } from 'react-router-dom';
import iconTester from '../../image/icon_tester.png'

import styles from './styles/index.module.css'

const LABELS = {
  TITLE_USER_PAGE: 'Личная страница',
  NAME_USER: 'Кузнецова Полина Сергеевна',
  GEN_USER: 'Жен',
  AGE_USER: '22 года',
  NUMBER: '89648750077',
  POST: 'Тестировщик',
}

interface Props {
  currentUser: {
    id: number,
    login: string,
    password: string,
  },
}
const UserPage: FC<Props> = ({ currentUser }) => (
  <div>
    <section className={ styles.wrapper_usesr_page }>
      <div className={ styles.wrapper_user_card }>
        <div className={ styles.wrapper_user_info }>
          <img alt="user_logo" src={ iconTester } className={ styles.wrapper_user_logo } />
          <div className={ styles.fio_info }>
            <strong>Логин:</strong>
            <p>{currentUser.login}</p>
          </div>
          <div className={ styles.number_info }>
            <Link to="/">
              <Button className={ styles.btn_exit } size="sm" colorScheme="red">Выход</Button>
            </Link>
          </div>
        </div>
      </div>
    </section>
  </div>
)

export default UserPage;
