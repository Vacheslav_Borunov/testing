import React, { FC, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {
  Card, CardHeader, CardBody, CardFooter, SimpleGrid, Heading, Text, Button,
} from '@chakra-ui/react'
import { Link } from 'react-router-dom';
import styles from './styles/index.module.css'

interface Props {
  taskQuestion: {
    idCategory: number,
    category: string,
    questionList : {
      idQuestion: number,
      question: string,
      checkedAnswer: number,
      answerList : {
        idAnswer: number,
        answer: string,
      }[],
    }[],
  }[],
}

const Home: FC<Props> = ({ taskQuestion }) => {
  const handleCategory = (idCategory:number) => {
    localStorage.setItem('idCategory', JSON.stringify(idCategory))
  }

  const renderCard = () => {
    if (taskQuestion.length !== 0) {
      return (
        taskQuestion.map(el => (
          <Card key={ el.idCategory } className={ styles.card_color }>
            <CardHeader>
              <Heading size="md">{el.category}</Heading>
            </CardHeader>
            <CardBody>
              {/* <Text>View a summary of all your customers over the last month.</Text> */}
            </CardBody>
            <CardFooter>
              <Link to="/taskPage">
                <Button colorScheme="yellow" onClick={ () => { handleCategory(el.idCategory) } }>Пройти тест</Button>
              </Link>
            </CardFooter>
          </Card>
        ))
      )
    }
    return null
  }

  return (
    <div className={ styles.wrapper_all }>
      <section className={ styles.wrapper_moveList }>
        <div className={ styles.wrapper_posters }>
          <SimpleGrid spacing={ 4 } templateColumns="repeat(auto-fill, minmax(200px, 1fr))">
            { renderCard() }
          </SimpleGrid>
        </div>
      </section>
    </div>
  );
}

export default Home;
