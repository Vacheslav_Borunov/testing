import React, { FC, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {
  Alert,
  AlertDescription,
  AlertIcon,
  AlertTitle,
  Input, InputGroup, InputRightElement, Button, ButtonGroup,
} from '@chakra-ui/react'
import { useNavigate } from 'react-router-dom';
import { observer } from 'mobx-react';
import styles from './styles/index.module.css'
import Card from '../../components/Card';

const CODE = '123'

interface Props {
  users: {
    id: number,
    login: string,
    password: string,
  }[],
  admins: {
    id: number,
    login: string,
    password: string,
  }[],
  setUsers: (params:{
    id: number,
    login: string,
    password: string,
  }[]) => void,
  setAdmins: (params:{
    id: number,
    login: string,
    password: string,
  }[]) => void,
  serCurrentUser : (params:{
    id: number,
    login: string,
    password: string,
  }) => void,
}

const Entry: FC<Props> = observer(({
  users,
  admins,
  setUsers,
  setAdmins,
  serCurrentUser,
}) => {
  const [ showPasswod, setShowPasswod ] = useState(false)
  const [ showCode, setShowCode ] = useState(false)
  const [ flagAlarmError, setFlagAlarmError ] = useState(false)

  const [ userLogin, setUserLogin ] = useState('')
  const [ userPassword, setUserPassword ] = useState('')
  // console.log('🚀 ~ userLogin в компаненте:', userLogin)
  // console.log('🚀 ~ userPassword в компаненте:', userPassword)

  const [ codeAdmin, setCodeAdmin ] = useState('')
  // console.log('🚀 ~ codeAdmin в компаненте', codeAdmin)

  const navigate = useNavigate();

  const handleClickPasswod = () => setShowPasswod(!showPasswod)
  const handleClickCode = () => setShowCode(!showCode)

  const handleRegistration = () => {
    const date = new Date().getTime()

    if (userLogin === '' || userPassword === '') {
      setFlagAlarmError(true)
    } else {
      setFlagAlarmError(false)

      if (codeAdmin !== CODE) {
        setUsers([ ...users, { id: date, login: userLogin, password: userPassword }])
        serCurrentUser({ id: date, login: userLogin, password: userPassword })
        localStorage.setItem('users', JSON.stringify([ ...users, {
          id: date, login: userLogin, password: userPassword,
        }]))
        localStorage.setItem('currentUser', JSON.stringify([{ id: date, login: userLogin, password: userPassword }]))

        setCodeAdmin('')
        navigate('/home');
        setFlagAlarmError(false)
      } else {
        serCurrentUser({ id: date, login: userLogin, password: userPassword })
        setAdmins([ ...admins, { id: date, login: userLogin, password: userPassword }])
        localStorage.setItem('admins', JSON.stringify([ ...admins, {
          id: date, login: userLogin, password: userPassword,
        }]))
        localStorage.setItem('currentUser', JSON.stringify([{ id: date, login: userLogin, password: userPassword }]))
      }

      navigate('/home');
      setFlagAlarmError(false)
    }
  }

  const handleEntry = () => {
    if (codeAdmin !== CODE) {
      const protectLogin = users.find(el => el.login === userLogin)
      if (protectLogin?.password === userPassword) {
        navigate('/home');
        serCurrentUser({ ...protectLogin })
        setFlagAlarmError(false)

        setCodeAdmin('')
      } else {
        setFlagAlarmError(true)
      }
    } else {
      const protectLogin = admins.find(el => el.login === userLogin)
      // console.log('🚀 ~ adminLogin:', adminLogin)
      // console.log('🚀 ~ admins:', admins)
      // console.log('🚀 ~ protectLogin:', protectLogin)
      if (protectLogin?.password === userPassword) {
        navigate('/home');
        serCurrentUser({ ...protectLogin })
        setFlagAlarmError(false)
      } else {
        setFlagAlarmError(true)
      }
    }
  }

  const alarmError = () => {
    if (flagAlarmError) {
      return (
        <div className={ styles.alarmMsg }>
          <Alert status="error">
            <AlertIcon />
            <AlertTitle>Вы ввели неверные данные для входа!</AlertTitle>
            <AlertDescription>Проверьте правильность пароля и логина и повторите попытку.</AlertDescription>
          </Alert>
        </div>
      )
    }
    return null
  }

  return (
    <div className={ styles.wrapper_all }>
      <section className={ styles.wrapper_entry }>
        <div className={ styles.wrapper_entry_user }>
          <p className={ styles.title }>Войдите или зарегестрируйтесь</p>
          <Input
            focusBorderColor="white.400"
            placeholder="Логин"
            onChange={ e => setUserLogin(e.target.value) }
          />
          <InputGroup size="md">
            <Input
              pr="4.5rem"
              focusBorderColor="white.400"
              type={ showPasswod ? 'text' : 'password' }
              placeholder="Пароль для входа"
              onChange={ e => setUserPassword(e.target.value) }
            />
            <InputRightElement width="4.5rem">
              <Button h="1.75rem" size="sm" onClick={ handleClickPasswod }>
                {showPasswod ? 'Hide' : 'Show'}
              </Button>
            </InputRightElement>
          </InputGroup>
          <div className={ styles.wrapper_btn }>
            <Button colorScheme="yellow" onClick={ () => handleEntry() }>Вход</Button>
            <Button colorScheme="yellow" onClick={ () => handleRegistration() }>Регистрация</Button>
          </div>
          <InputGroup size="md">
            <Input
              pr="4.5rem"
              focusBorderColor="white.400"
              type={ showCode ? 'text' : 'password' }
              placeholder="Код(только для администраторов)"
              onChange={ e => setCodeAdmin(e.target.value) }
            />
            <InputRightElement width="4.5rem">
              <Button h="1.75rem" size="sm" onClick={ handleClickCode }>
                {showCode ? 'Hide' : 'Show'}
              </Button>
            </InputRightElement>
          </InputGroup>
          { alarmError() }
        </div>
      </section>
    </div>
  );
})

export default Entry;
