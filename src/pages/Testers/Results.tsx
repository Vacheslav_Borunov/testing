// @ts-nocheck
import React, { FC, useEffect, useState } from 'react';

import {
  Alert,
  AlertDescription,
  AlertIcon,
  AlertTitle,
  Button, Input, InputGroup, InputLeftAddon, NumberDecrementStepper, NumberIncrementStepper, NumberInput, NumberInputField, NumberInputStepper, Radio, RadioGroup, Stack,
} from '@chakra-ui/react';
import iconTester from '../../image/icon_tester.png'

import styles from './styles/index.module.css'

const Results: FC<Props> = ({ taskPassedQuestion }) => {
  const [ currentTest, setCurrentTest ] = useState([])

  const hendaleCurrentTest = idPassed => {
    const filterCurrentTest = taskPassedQuestion.find(el => el.idPassed === idPassed)
    setCurrentTest(filterCurrentTest)
  }

  const calcAnswer = questionList => {
    const questionRight = questionList.filter(el => el.answerUser === el.checkedAnswer)
    return `${questionRight.length} из ${questionList.length}`
  }

  const renderItem = (answerListItem, checkedAnswer) => (
    answerListItem.map((el, index) => (
      <div className={ styles.radioBlock } key={ el.idAnswer }>
        <Radio isDisabled className={ styles.radio } colorScheme="green" value={ `${index + 1}` }>
          <p className={ checkedAnswer === `${index + 1}` ? styles.color_text_green : styles.color_text_red }>{el.answer}</p>
        </Radio>
      </div>
    )))

  const renderTest = () => (
    <div className={ styles.wrapper_question_list }>
      <p className={ styles.wrapper_title }>{currentTest.category}</p>
      { currentTest.questionList?.map(el => (
        <div key={ el.idQuestion } className={ styles.wrapper_question_list }>
          <strong>{el.question}</strong>
          <RadioGroup defaultValue={ el.answerUser }>
            <Stack>
              { renderItem(el.answerList, el.checkedAnswer) }
            </Stack>
          </RadioGroup>
        </div>
      )) }
    </div>
  )

  const renderUser = () => {
    if (taskPassedQuestion.length > 0) {
      return (
        taskPassedQuestion.map(el => (
          <div key={ el.idPassed } className={ styles.wrapper_user_card }>
            <img alt="user_logo" src={ iconTester } className={ styles.wrapper_user_logo } />
            <div>
              <strong>Логин:</strong>
              <p>{el.loginUser}</p>
            </div>
            <div>
              <strong>Категория:</strong>
              <p>{el.category}</p>
            </div>
            <div>
              <strong>Результат:</strong>
              <p>{calcAnswer(el.questionList)}</p>
            </div>
            <Button colorScheme="yellow" onClick={ () => hendaleCurrentTest(el.idPassed) }>
              Подробнее
            </Button>
          </div>
        ))
      )
    }

    return <p>Никто пока не прошёл тест</p>
  }

  return (
    <div>
      <section className={ styles.wrapper_moveList }>
        <p className={ styles.wrapper_title }>Результаты</p>
        <div className={ styles.wrapper_content }>
          <div className={ styles.wrapper_content_user }>
            { renderUser() }
          </div>
          <div className={ styles.wrapper_content_test }>
            { renderTest() }
          </div>
        </div>
      </section>
    </div>
  );
}

export default Results;
