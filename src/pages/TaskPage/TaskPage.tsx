// @ts-nocheck
import React, { FC, useEffect, useState } from 'react';
import PropTypes, { bool, number } from 'prop-types';
import { observer } from 'mobx-react'
import {
  Button, Input, Select, Textarea,
  Checkbox,
  AlertIcon,
  AlertDescription,
  AlertTitle,
  Alert,
  RadioGroup,
  Stack,
  Radio,
} from '@chakra-ui/react';
import styles from './styles/index.module.css'

// interface Props {
//   taskQuestion: {
//     idCategory: number,
//     category: string,
//     questionList : {
//       idQuestion: number,
//       question: string,
//       checkedAnswer: number,
//       answerList : {
//         idAnswer: number,
//         answer: string,
//       }[],
//     }[],
//   }[],
//   }

//   type questionList = {
//     taskQuestion: {
//     idCategory: number,
//     category: string,
//     questionList : {
//       idQuestion: number, ид каждого
//       question: string,
//       checkedAnswer: number,
//       answerList : {
//         idAnswer: number,
//         answer: string,
//       }[],
//     }[],
//   }[],
//   }

const TaskPage: FC<Props> = observer(({
  taskQuestion = [],
  currentUser = {},
  taskPassedQuestion = [],
  setTaskPassedQuestion,
}) => {
  const [ flagAlarmError, setFlagAlarmError ] = useState(false)
  const [ category, setCategory ] = useState('')
  const [ questionList, setQuestionList ] = useState<questionList>([])
  const [ succsesTestFlag, setSuccsesTestFlag ] = useState(false)
  const [ passedTask, setPassedTask ] = useState([])

  useEffect(() => {
    const localIdCategory = Number(localStorage.getItem('idCategory'))

    if (taskQuestion) {
      const taskQuestionFilter = taskQuestion.find(el => el.idCategory === localIdCategory)
      setCategory(taskQuestionFilter?.category)
      setQuestionList(taskQuestionFilter?.questionList)
    }
  }, [])
  // questionList - массив без ответов(начальный), passedTask - растущий массив уже с ответами
  const handlePassed = (valuePassed, idQuestion) => {
    const passedQuestion = questionList.find(el => el.idQuestion === idQuestion)// ищем объект в начальном массиве
    const idxPassedQuestion = questionList.findIndex(el => el.idQuestion === idQuestion)// ищем индекс оюъекта из начального массива

    if (passedTask[idxPassedQuestion]?.answerUser) {
      const newPassedQuestion = passedTask.slice()
      newPassedQuestion[idxPassedQuestion] = {
        idQuestion: passedQuestion.idQuestion,
        question: passedQuestion.question,
        checkedAnswer: passedQuestion.checkedAnswer,
        answerUser: valuePassed,
        answerList: passedQuestion.answerList,
      }

      setPassedTask(newPassedQuestion)
    } else {
      setPassedTask([ ...passedTask, {
        idQuestion: passedQuestion.idQuestion,
        question: passedQuestion.question,
        checkedAnswer: passedQuestion.checkedAnswer,
        answerUser: valuePassed,
        answerList: passedQuestion.answerList,
      }])
    }
  }

  const hendaleEndTest = () => {
    setSuccsesTestFlag(true)
    const localIdCategory = Number(localStorage.getItem('idCategory'))
    const date = new Date().getTime()

    if (questionList.length === passedTask.length) {
      setFlagAlarmError(false)
      setTaskPassedQuestion([ ...taskPassedQuestion, {
        idPassed: date,
        idCategory: localIdCategory,
        loginUser: currentUser.login,
        category,
        questionList: passedTask,
      },
      ])

      localStorage.setItem('taskPassedQuestion', JSON.stringify([ ...taskPassedQuestion, {
        idPassed: date,
        idCategory: localIdCategory,
        loginUser: currentUser.login,
        category,
        questionList: passedTask,
      }]))
    } else {
      setFlagAlarmError(true)
    }
  }

  const alarmError = () => {
    if (flagAlarmError) {
      return (
        <Alert status="error">
          <AlertIcon />
          <AlertTitle>Вы ответили не на все вопросы!</AlertTitle>
          <AlertDescription>Заполните все поля и повторите попытку.</AlertDescription>
        </Alert>
      )
    }
    return null
  }

  const succsesTest = () => (
    <Alert
      status="success"
      variant="subtle"
      flexDirection="column"
      alignItems="center"
      justifyContent="center"
      textAlign="center"
      height="200px"
    >
      <AlertIcon boxSize="40px" mr={ 0 } />
      <AlertTitle mt={ 4 } mb={ 1 } fontSize="lg">
        Тест пройден!
      </AlertTitle>
      <AlertDescription maxWidth="sm">
        Спасибо, через некоторое время с вами свяжутся для обратной связи.
      </AlertDescription>
    </Alert>
  )

  const renderItem = (answerListItem, idQuestion) => (
    answerListItem.map((el, index) => (
      <div className={ styles.radioBlock } key={ el.idAnswer }>
        <Radio className={ styles.radio } colorScheme="green" value={ `${index + 1}` } onChange={ e => handlePassed(e.target.value, idQuestion) }>{el.answer}</Radio>
      </div>
    )))

  const renderQuestion = () => (
    questionList?.map(el => (
      <div key={ el.idQuestion }>
        <strong>{el.question}</strong>
        <RadioGroup className={ styles.question_block }>
          <Stack>
            { renderItem(el.answerList, el.idQuestion) }
          </Stack>
        </RadioGroup>
      </div>
    ))
  )

  const renderContent = () => {
    if (succsesTestFlag && !flagAlarmError) {
      return succsesTest()
    }

    return (
      <section className={ styles.wrapper_moveList }>
        { alarmError() }
        <div className={ styles.wrapper_posters }>
          <p className={ styles.wrapper_title }>{category}</p>
          <div className={ styles.wrapper_content }>
            { renderQuestion() }
          </div>
        </div>
        <Button colorScheme="yellow" onClick={ () => hendaleEndTest() }>
          Завершить тест
        </Button>
      </section>
    )
  }

  return (
    <div>
      {renderContent()}
    </div>
  )
})

export default TaskPage;
