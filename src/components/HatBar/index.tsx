/* eslint-disable jsx-a11y/mouse-events-have-key-events */
import React, { FC, useCallback } from 'react';
import { Link } from 'react-router-dom'
import { observer } from 'mobx-react';
import logoApp from '../../image/logo_app.png'
import iconTester from '../../image/icon_tester.png'

import styles from './styles/index.module.css'

interface Props {
  currentUserId: number,
  admins: {
    id: number,
    login: string,
    password: string,
  }[],
}

const HatBar: FC<Props> = observer(({ currentUserId = 0, admins = [] }) => {
  const protectId = admins.find(el => el.id === currentUserId)
  const navigationHeader = () => {
    if (protectId) {
      return (
        <ul>
          <li><Link to="/createTask">Создание нового теста</Link></li>
          <li><Link to="/results">Результаты</Link></li>
          {/* <li><Link to="/managers">Руководители</Link></li> */}
        </ul>
      )
    }
    return null
  }

  return (
    <header className={ styles.wrapper_header }>
      <Link to="/home" className={ styles.wrapper_logo }>
        <img alt="logo_app" src={ logoApp } className={ styles.logo_app } />
        <p className={ styles.label_app }>Yes Test</p>
      </Link>
      <div className={ styles.wrapper_nav_search }>
        <nav className={ styles.wrapper_nav }>
          { navigationHeader() }
        </nav>
      </div>
      <div className={ styles.wrapper_infoUser }>
        <Link to="/userPage">
          <img alt="iconUser" src={ iconTester } className={ styles.icon_User } />
        </Link>
      </div>
    </header>
  )
})

export default HatBar;
